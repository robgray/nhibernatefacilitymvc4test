﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.Facilities.NHibernate;
using Castle.Transactions;
using Persistence;
using Models;

namespace NHibernateFacilityMVC4Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPersonRepository _personRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ISessionManager _sessionManager;

        public HomeController(IPersonRepository personRepository, ICourseRepository courseRepository, ISessionManager sessionManager)
        {
            _personRepository = personRepository;
            _courseRepository = courseRepository;
            _sessionManager = sessionManager;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            
            return View();
        }

        public ActionResult About()
        {
            var person = new Person {FirstName = "Rob", LastName = "Gray"};
            _personRepository.Save(person);
            
            return View();
        }

        public virtual ActionResult Contact()
        {
            using (var session1 = _sessionManager.OpenSession())
            {
                var person = session1.Get<Person>(1);
                using (var session2 = _sessionManager.OpenSession())
                {
                    var person2 = session2.Get<Person>(1);
                }                
            }

            //var person = _personRepository.Get(1);
            //var person2 = _personRepository.Get(1);
            //if (person != null)
            //{
            //    person.LastName = DateTime.Now.ToLongTimeString();
            //}

            return View();
        }
    }
}
