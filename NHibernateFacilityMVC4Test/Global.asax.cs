﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NHibernateFacilityMVC4Test.App_Start;
using log4net;
using log4net.Config;

namespace NHibernateFacilityMVC4Test
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer Container;
        protected ILog AppLogger { get; set; }

        protected void Application_Start()
        {
            AppDomain.CurrentDomain.UnhandledException += Application_Error;
            try
            {
                XmlConfigurator.Configure();
                AppLogger = LogManager.GetLogger(GetType());
                AppLogger.Info("Application_Start...");

                AppLogger.Info("Bootstrapping the container...");
                Container = ContainerConfig.Config(AppLogger);

                AppLogger.Info("Setting the Controller Factory...");
                var controllerFactory = new WindsorControllerFactory(Container.Kernel);
                ControllerBuilder.Current.SetControllerFactory(controllerFactory);

                AreaRegistration.RegisterAllAreas();

                WebApiConfig.Register(GlobalConfiguration.Configuration);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                AuthConfig.RegisterAuth();                
            }
            catch (Exception ex)
            {
                AppLogger.Fatal("Application_Start FAILED", ex);
                throw;
            }
        }

        void Application_Error(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = Server.GetLastError();

            // Don't log client http errors.
            var httpException = exception as HttpException;
            if (httpException != null && httpException.GetHttpCode() >= 400 && httpException.GetHttpCode() <= 499)
                return;

            if (AppLogger != null)
            {
                AppLogger.Error("Exception caught in global exception handler.", Server.GetLastError());
            }
        }

        protected void Application_End()
        {
            Container.Dispose();            
        }
    }
}