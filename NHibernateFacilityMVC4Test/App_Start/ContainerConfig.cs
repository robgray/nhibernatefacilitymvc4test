﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using log4net;


namespace NHibernateFacilityMVC4Test.App_Start
{
    public class ContainerConfig
    {
        private static IWindsorContainer _container;

        public static IWindsorContainer Config(ILog logger)
        {
            _container = new WindsorContainer();
            _container.AddFacility<LoggingFacility>(x => x.UseLog4Net());            
            _container.Install(FromAssembly.This());

            return _container;
        }
    }
}