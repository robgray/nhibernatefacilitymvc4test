﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.Facilities.AutoTx;
using Castle.Facilities.NHibernate;
using Castle.MicroKernel.Registration;
using Castle.Windsor.Installer;
using NHibernate;
using Persistence;

namespace NHibernateFacilityMVC4Test.Installers
{
    public class PersistenceInstaller : IWindsorInstaller
    {
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.AddFacility<AutoTxFacility>();
            container.Register(Component.For<INHibernateInstaller>().ImplementedBy<NHibernateInstaller>());//.LifeStyle.Singleton);

            var ni = container.Resolve<INHibernateInstaller>();
            var t = container.ResolveAll<IInterceptor>();
            
            //container.AddFacility<NHibernateFacility>();
            container.AddFacility<NHibernateFacility>(f => f.DefaultLifeStyle = DefaultSessionLifeStyleOption.SessionPerWebRequest);

            container.Install(FromAssembly.Containing<PersonRepository>());
        }
    }
}