using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
[assembly: AssemblyTitle("Castle NHibernate Facility")]
[assembly: AssemblyDescription("Easy NHibernate integration with declarative transactions using Castle Transaction Services and .Net System.Transactions. Integrate Transactional NTFS with NHibernate and database transactions, or choose methods to fork dependent transactions for to run your transaction constituents in parallel. The NHibernate Facility is configured using FluentNHibernate git unavailable - 2013-01-04 104526")]
[assembly: AssemblyProduct("Castle NHibernate Facility")]
[assembly: AssemblyCopyright("Apache 2.0 Licensed. Copyright: Henrik Feldt 2011, The Castle Project")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("0.7.1.00410")]
[assembly: AssemblyFileVersion("0.7.1.00410")]

[assembly: AssemblyInformationalVersion("0.7.1.00410")]
[assembly: CLSCompliantAttribute(false)]
[assembly: AssemblyConfiguration("Release")]
[assembly: Guid("7f4d3ca3-95d5-4557-bbf5-1df3dda08744")]
