﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public interface IRepository<T>
    {
        T Get(int id);

        T Save(T entity);    
    }
}
