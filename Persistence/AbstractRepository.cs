﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Facilities.NHibernate;
using Models;
using NHibernate;

namespace Persistence
{
    public abstract class AbstractRepository<T> : IRepository<T>
    {
        private readonly ISessionManager _sessionManager;

        protected AbstractRepository(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public T Get(int id)
        {
            return Session.Get<T>(id);
        }

        public T Save(T entity)
        {
            Session.SaveOrUpdate(entity);
            return entity;
        }

        public ISession Session
        {
            get { return _sessionManager.OpenSession(); }
        }
    }
}
