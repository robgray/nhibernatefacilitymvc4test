﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Castle.Facilities.NHibernate;

namespace Persistence
{
    public class CourseRepository : AbstractRepository<Course>, ICourseRepository
    {
        public CourseRepository(ISessionManager sessionManager) : base(sessionManager) { }
        
    }
}
