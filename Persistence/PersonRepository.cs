﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Facilities.NHibernate;
using Models;
using NHibernate;

namespace Persistence
{
    public class PersonRepository : AbstractRepository<Person>, IPersonRepository
    {
        public PersonRepository(ISessionManager sessionManager) : base(sessionManager) { }        
    }
}
