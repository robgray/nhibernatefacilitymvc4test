﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;

namespace Persistence.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                        .BasedOn(typeof(IRepository<>))
                        .WithService.DefaultInterfaces()
                        .LifestylePerWebRequest());            
        }
    }
}
