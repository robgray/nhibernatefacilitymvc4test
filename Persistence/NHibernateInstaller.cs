﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Facilities.NHibernate;
using Castle.Transactions;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System.Configuration;
using Persistence.Mappings;

namespace Persistence
{
    public class NHibernateInstaller : INHibernateInstaller
    {
        private readonly Maybe<IInterceptor> _Interceptor;

        public NHibernateInstaller()
        {
            _Interceptor = Maybe.None<IInterceptor>();
        }

        public NHibernateInstaller(IInterceptor interceptor)
        {
            _Interceptor = Maybe.Some(interceptor);
        }

        public FluentConfiguration BuildFluent()
        {
            return Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.DefaultSchema("dbo")
                .ConnectionString(ConfigurationManager.ConnectionStrings["TestDb"].ConnectionString))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<PersonMap>());
        }

        public Maybe<IInterceptor> Interceptor
        {
            get
            {
                return _Interceptor;
            }
        }

        public bool IsDefault
        {
            get { return true; }
        }

        public void Registered(ISessionFactory factory)
        {
            
        }

        public string SessionFactoryKey
        {
            get { return "sf.default"; }
        }
    }
}
