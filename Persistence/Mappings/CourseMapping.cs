﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Models;

namespace Persistence.Mappings
{
    public class CourseMapping : ClassMap<Course>
    {
        public CourseMapping()
        {
            Id(x => x.Id);
            Map(x => x.Name);
        }
    }
}
